# random password
# shuf -n5 /usr/share/dict/words
# < /dev/urandom tr -dc _@#$%A-Z-a-z-0-9 | head -c32

# rsync files
#rsync -avr --rsh='ssh -p2222' /path/to/local/files userName@website.com:/path/to/destination/

alias e="emacs -nw"
alias arduino="~/proj/tools/arduino/arduino &"

alias ll="ls -lhA"
alias lsl="ls -lhFA | less"
alias l.='ls -d .* --color=auto'
# ls sort by time
alias lt="ls -ltr "
# ls sort by byte size
alias lS='ls -Slr'
# Latest files
#alias lt=ls -alrt 
cdl()    {
  cd "$@";
  ls -al;
}

alias ..="cdl .."
alias ...="cdl ../.."
alias ff="find . -name "
#alias ducks=du -ck | sort -nr | head

#alias psg=ps -He | grep -v $$ | grep -i -e WCHAN -e 
alias path='echo -e ${PATH//:/\\n}'
alias now='date +"%T"'
alias nowtime=now
alias nowdate='date +"%d-%m-%Y"'

alias del='rm -I --preserve-root'

alias df="df -Tha --total"
alias du="du -ach | sort -h"
alias free="free -mt"
alias ps="ps auxf"
alias psg="ps aux | grep -v grep | grep -i -e VSZ -e"
alias mkdir="mkdir -pv"
alias wget="wget -c"
alias histg="history | grep"
alias myip="curl http://ipecho.net/plain; echo"
mkcd () {
    mkdir -p $1
    cd $1
}


# if user is not root, pass all commands via sudo #
if [ $UID -ne 0 ]; then
    alias reboot='sudo reboot'
    alias update='sudo apt-get update && sudo apt-get upgrade && sudo apt-get autoremove'
    alias qq='sudo shutdown -h now'
fi

# Reboot my home Linksys WAG160N / WAG54 / WAG320 / WAG120N Router / Gateway from *nix.
alias rebootlinksys="curl -u 'admin:my-super-password' 'http://192.168.1.2/setup.cgi?todo=reboot'"
 
# Reboot tomato based Asus NT16 wireless bridge
alias reboottomato="ssh admin@192.168.1.1 /sbin/reboot"

# Show file without comments
#alias nocomment='grep -Ev '''^(#|$)'''

# Serve a directory on a given port
# $1 = port
# Example: servedir 8080
servedir() {
  # Allow myself to change the port ($1)
  python -m SimpleHTTPServer "$1"
}

# Copy to clipboard
# grep filename | pbcopy
alias pbcopy='xsel --clipboard --input'
alias pbpaste='xsel --clipboard --output'

# Backup file 
#bu() { cp "$1" "$1".backup-`date +%y%m%d`; }
backupdate() { cp "$1" "$1".$(date +"%y%m%d%H%M"); }
backuptstamp() { echo $1;cp "$1" "$1".$(date +"%s"); }

#radio
#alias dlf=”/usr/local/bin/mplayer -nocache -audiofile-cache 64 -prefer-ipv4 $(GET http://www.dradio.de/streaming/dlf.m3u|head -1)”

# scrape images
scrapeimages() {
  wget -nd -H -p -A jpg,jpeg,png,gif -e robots=off $1
}

findrpi() {
  sudo nmap -sP 192.168.0.0/24 | awk '/^Nmap/{ip=$NF}/B8:27:EB/{print ip}'
}
